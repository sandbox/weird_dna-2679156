<?php
/**
 * @file
 * Contains  * Contains \Drupal\graphql_api\Form\GraphQLConfigForm.

 */

namespace Drupal\graphql_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\graphql_api\Sample\UserResolver;

/**
 * Implements an example form.
 */
class GraphQLConfigForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'graphql_api_config_form';
  }

  public function toCamelCase($string){
    $properString = $this->toProperCase($string);
    $first_letter = substr($properString,0,1);
    $remainingCharacters = substr($properString,1,strlen($properString));
    return strtolower($first_letter).$remainingCharacters;
  }
  public function toProperCase($string){
    $string = str_replace(" ","_",$string);
    $string_parts = explode("_",$string);
    $proper_string_parts = array();
    foreach($string_parts as $string_part){
      $first_letter = substr($string_part,0,1);
      $remainingCharacters = substr($string_part,1,strlen($string_part));
      $proper_string_parts[] = strtoupper($first_letter).$remainingCharacters;
    }
    return implode("",$proper_string_parts);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $moduleHandler = \Drupal::service('module_handler');
    $services = $moduleHandler->getModuleList();
    $availableResolvers = array();
    foreach($services as $extension){
      $root = $extension->getRoot();
      $pathName = $extension->getPathName();
      $lastSlash = strrpos($pathName,'/');
      $path = substr($pathName,0,$lastSlash);
      $resolverDirectory = "$path/src/Resolvers";
      if (file_exists($resolverDirectory)){
        if (is_dir($resolverDirectory)){
          $files = scandir($resolverDirectory);
          foreach($files as $file){
            if (strpos(strtolower($file),".php") > 0){
              $namespace = 'Drupal\\' .
                str_replace("sites/all/","",str_replace("modules/","",str_replace("custom/","",$path))) .
                "\\Resolvers\\" . str_replace(".php","",$file);
              $availableResolvers[] = $namespace;
            }
          }
        }
      }
    }

    $entities =  \Drupal::entityManager()->getAllBundleInfo();
    //$config = \Drupal::config('graphql.config');
    foreach($entities as $entity_type=>$entity_list){
      if (($entity_type == "user") || ($entity_type == "node")){
        $form[$entity_type]["label"] = array(
          "#type" => 'label',
          "#title" => $entity_type
        );
        foreach($entity_list as $name=>$value){
          $form[$name]["label"] = array(
            "#type" => 'label',
            "#title" => $name
          );
          $form[$name]["exposed"] = array(
            "#type" => 'checkbox',
            "#title" => 'Exposed'
          );
          $form[$name]["type_name"] = array(
            "#type" => 'textfield',
            "#title" => 'GraphQL Type Name',
            '#value' => $this->toProperCase($name)
          );
          $form[$name]["query_name"] = array(
            "#type" => 'textfield',
            "#title" => 'Singular Query Field Name',
            '#value' => $this->toCamelCase($name)
          );
          $form[$name]["plural_name"] = array(
            "#type" => 'textfield',
            "#title" => 'Plural Query Field Name',
            '#value' => $this->toCamelCase($name)."s"
          );
          $form[$name]["connection_type"] = array(
            "#type" => 'select',
            "#title" => 'Connection Type',
            '#options' => array('DIRECT','EDGES','BOTH')
          );
          $form[$name]["fields"]["label"] = array(
            "#type" => 'label',
            "#title" => "Fields"
          );
          $ids = \Drupal::entityQuery('field_config')
            ->condition('id', "$entity_type.$name.", 'STARTS_WITH')
            ->execute();
          $field_configs = \Drupal\field\Entity\FieldConfig::loadMultiple($ids);
          foreach ($field_configs as $field_instance) {
            $r = new \stdClass();
            $fieldName = $field_instance->getLabel();
            $form[$name]["fields"][$fieldName]["label"] = array(
              "#type" => 'label',
              "#title" => $fieldName
            );
            $form[$name]["fields"][$fieldName]["exposed"] = array(
              "#type" => 'checkbox',
              "#title" => 'Exposed?'
            );
            $form[$name]["fields"][$fieldName]["fieldName"] = array(
              "#type" => 'textfield',
              "#title" => 'Field Name',
              "#value" => $this->toCamelCase($fieldName)
            );
          }
        }
      }
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('phone_number')) < 3) {
      $form_state->setErrorByName('phone_number', $this->t('The phone number is too short. Please enter a full phone number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state->getValue('phone_number'))));
  }

}
